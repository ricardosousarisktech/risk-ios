//
//  HomeViewController.m
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()


-(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;

-(void)setDateStrings;





@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.navigationController.navigationBar.hidden = YES;
    
//    self.navigationController.navigationBarHidden = NO;
    
    
   //week select setup NEED to move to class
    
 
//    _weekStrings = @[@"This Week",@"Last Week",@"Week -2",@"Week -3",@"Week -4",@"Week -5",@"Week -6",@"Week -7",@"Week -8",@"Week -9",@"Week -10",@"Week -11",@"Week -12",@"Week -13",@"Week -14",@"Week -15"];
    _weekOffset = 1;

    // Initialize Data
    _pickerData = @[@"Item 1", @"Item 2", @"Item 3", @"Item 4", @"Item 5", @"Item 6"];
    
    
    [self setDateStrings];

    
    
    UISwipeGestureRecognizer *swipeRightWeek = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToRightWithGestureRecognizer:)];
    swipeRightWeek.direction = UISwipeGestureRecognizerDirectionRight;
    
    UISwipeGestureRecognizer *swipeLeftWeek = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideToLeftWithGestureRecognizer:)];
    swipeLeftWeek.direction = UISwipeGestureRecognizerDirectionLeft;

    [self.swipeWeekControl addGestureRecognizer:swipeRightWeek];
    [self.swipeWeekControl addGestureRecognizer:swipeLeftWeek];
    
    
    // Connect picker view data
/*
    self.IMEIpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.IMEIpicker.showsSelectionIndicator = YES;
    self.IMEIpicker.dataSource = self;
    self.IMEIpicker.delegate = self;
    [self.view addSubview:self.IMEIpicker];
*/


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer withDataSource:(NSMutableArray *) dataArray

-(void)slideToRightWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [UIView animateWithDuration:0.2 animations:^{
        self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, 320.0, 0.0);
        
   
        
 
    }
                     completion:^(BOOL finished) {
                         NSLog(@"Right Is completed");
                         self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, -640.0, 0.0);
                         _weekOffset = _weekOffset + 1;
                         if (_weekOffset >= [_weekRangeStrings count]){
                             _weekOffset = [_weekRangeStrings count] - 1;
                         }
                         self.weekLabel.text = _weekRangeStrings[_weekOffset];
                         
                         [UIView animateWithDuration:0.2 animations:^{
                             self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, 320.0, 0.0);
                         }];
                    }];
    
   }

-(void)slideToLeftWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer{
    [UIView animateWithDuration:0.2 animations:^{
       self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, -320.0, 0.0);
        
   
    }
                     completion:^(BOOL finished) {
                         NSLog(@"Left Is completed");
                         self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, 640.0, 0.0);
                         _weekOffset = _weekOffset - 1;
                         if (_weekOffset < 0 ){
                             _weekOffset = 0;
                         }

                         self.weekLabel.text = _weekRangeStrings[_weekOffset];

                         [UIView animateWithDuration:0.2 animations:^{
                             self.swipeWeekControl.frame = CGRectOffset(self.swipeWeekControl.frame, -320.0, 0.0);
                         }];
                     }];
     
    
   
}



-(void)setDateStrings{
   
    //note currently creates a mutableArray with strings for controller, will need to create search strings either from these strings or calculate and store in method below

    
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dayOfWeekFormatter = [[NSDateFormatter alloc] init] ;
    [dayOfWeekFormatter setDateFormat:@"F"];
    NSString *dayOfWeekString = [dayOfWeekFormatter stringFromDate:now];  //string of value 0,1,2,3,4,5,6
//    NSLog(dayOfWeekString);
 
    int dayOfWeek = [dayOfWeekString intValue];
 //   NSLog(@"Today is %d",dayOfWeek);
    
    NSDateFormatter *dateNoTime = [[NSDateFormatter alloc] init];
    [dateNoTime setDateFormat:@"d/M"];                                      //note mm means minutes !!!
    
    NSString *endRangeAsString = [dateNoTime stringFromDate: now];          //end of this weeks range to be displayed -remember to add time API method
    
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    comps.day = -dayOfWeek;                                         //get back to Sunday
    NSCalendar* calendar =[NSCalendar currentCalendar];
    NSDate *startRange =[calendar dateByAddingComponents: comps toDate:now options:0];
    NSString *startRangeAsString = [dateNoTime stringFromDate: startRange];
    NSString *currentWeekString = [NSString stringWithFormat:@"Week %@ - %@",startRangeAsString,endRangeAsString ];
    NSLog(@"The current week %@",currentWeekString);

    
 
    
    _weekRangeStrings = [NSMutableArray arrayWithObjects:currentWeekString, nil];    //initialise with first value
  
/*
    //just go back 16 weeks for now superceeded with a do loop that check against start date
    
    for ( int i=0; i < 16; i++ ){
        
        comps.day = -1-(i*7);
        NSDate *endRangeW2 =[calendar dateByAddingComponents: comps toDate:startRange options:0];
        NSString *endRangeW2AsString = [dateNoTime stringFromDate: endRangeW2];
        
        comps.day = -7-(i*7);
        NSDate *startRangeW2 =[calendar dateByAddingComponents: comps toDate:startRange options:0];
        NSString *startRangeW2AsString = [dateNoTime stringFromDate: startRangeW2];
        
        NSString *lastWeekString = [NSString stringWithFormat:@"Week %@ - %@",startRangeW2AsString,endRangeW2AsString ];
 //       NSLog(lastWeekString);
        [_weekRangeStrings addObject:lastWeekString]; // add the previous week into the mutablearray

    }
*/
 
    //stub create a Sign-up Date
    
    NSDateComponents* compsForSignup = [[NSDateComponents alloc] init];
    compsForSignup.day = -36;
    NSDate *signupDate = [calendar dateByAddingComponents: compsForSignup toDate:startRange options:0];
    //
    
    int i = 0;
    NSDate *startRangeW2 = startRange;   //[calendar dateByAddingComponents: comps toDate:startRange options:0];
    do {
        comps.day = -1-(i*7);
        NSDate *endRangeW2 =[calendar dateByAddingComponents: comps toDate:startRange options:0];
        NSString *endRangeW2AsString = [dateNoTime stringFromDate: endRangeW2];
        
        comps.day = -7-(i*7);
     //   NSDate *startRangeW2 =[calendar dateByAddingComponents: comps toDate:startRange options:0];
        startRangeW2 =[calendar dateByAddingComponents: comps toDate:startRange options:0];
        NSString *startRangeW2AsString = [dateNoTime stringFromDate: startRangeW2];
        
        NSString *lastWeekString = [NSString stringWithFormat:@"Week %@ - %@",startRangeW2AsString,endRangeW2AsString ];
        //       NSLog(lastWeekString);
        [_weekRangeStrings addObject:lastWeekString]; // add the previous week into the mutablearray
        
        i++;
    }
    
    while ([startRangeW2 compare:signupDate] == NSOrderedDescending);
    
    
    
    
   
}


// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
   // NSString *item = [yourItems objectAtIndex:row];
    
   // return item;
    
    return _pickerData[row];
}

/*
 //properties
 
 @property (strong, nonatomic) UIDatePicker *theDatePicker;
 @property (strong, nonatomic) UIView *pickerView;
 - (IBAction)createDatePickerAndShow:(id)sender;

 */

- (IBAction)numberPlatePressed:(id)sender {
    
    [self createDatePickerAndShow] ;
    
}



 
-(void) createDatePickerAndShow {
 
 /*
 
 UIToolbar *controlToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, _picker.bounds.size.width, 44)];
 
 [controlToolbar sizeToFit];
 
 UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
 
 //............add this to disable the warning of @selector(dismissDateSet)]
 
 #pragma GCC diagnostic ignored "-Wundeclared-selector"
 
 UIBarButtonItem *setButton = [[UIBarButtonItem alloc] initWithTitle:@"Set" style:UIBarButtonItemStyleDone target:self action:@selector(dismissDateSet)];
 
 //...........Edit to UIBarButtonItemStylePlain that is supported in ios 8
 
 UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelDateSet)];
 
 [controlToolbar setItems:[NSArray arrayWithObjects:spacer, cancelButton, setButton, nil] animated:NO];
 
 
 */



// [_theDatePicker setFrame:CGRectMake(0, controlToolbar.frame.size.height - 15, _theDatePicker.frame.size.width,_theDatePicker.frame.size.height)];

if (!_picker) {
    _picker = [[UIPickerView alloc] initWithFrame:_theDatePicker.frame];    //may need UIView aand change .h property
} else {
    
    //.................here edit _pickerView
    
    
    [_picker setHidden:NO];
}


CGFloat pickerViewYpositionHidden = self.view.frame.size.height + _picker.frame.size.height;

CGFloat pickerViewYposition = self.view.frame.size.height - _picker.frame.size.height;

[_picker setFrame:CGRectMake(_picker.frame.origin.x, pickerViewYpositionHidden, _picker.frame.size.width,_picker.frame.size.height)];

[_picker setBackgroundColor:[UIColor whiteColor]];
// [_picker addSubview:controlToolbar];

// [_picker addSubview:_theDatePicker];
// [_theDatePicker setHidden:NO];

self.IMEIpicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
self.IMEIpicker.showsSelectionIndicator = YES;
self.IMEIpicker.dataSource = self;
self.IMEIpicker.delegate = self;
[_picker addSubview:self.IMEIpicker];         //[self.view addSubview:self.IMEIpicker];

[self.IMEIpicker setHidden:NO];



[self.view addSubview:_picker];

[UIView animateWithDuration:0.5f
                 animations:^{
                     [_picker setFrame:CGRectMake(_picker.frame.origin.x,
                                                  pickerViewYposition,
                                                  _picker.frame.size.width,
                                                  _picker.frame.size.height)];
                 }
                 completion:nil];

}

 




 -(void) dismissDateSet {
  NSLog(@"Dismiss");
        
 }

 -(void) cancelDateSet {
     
     NSLog(@"Cancel");
     
     CGFloat pickerViewYpositionHidden = self.view.frame.size.height + _picker.frame.size.height;
     
     [UIView animateWithDuration:0.5f
                      animations:^{
                        [_picker setFrame:CGRectMake(_picker.frame.origin.x,
                        pickerViewYpositionHidden,
                        _picker.frame.size.width,
                        _picker.frame.size.height)];
                      }
                      completion:nil];
 }
 
 //http://stackoverflow.com/questions/25923241/what-is-best-solution-to-add-a-uipickerview-inside-a-uiactionsheet-in-ios8
 

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
}


#pragma mark - Navigation

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/*
 self.navigationController popViewControllerAnimated:YES];
 
 
 nstead of using a segue to go back to the previous controller you should use the event that was associated with the segue going back to instead dismiss the current view controller (if segue was modal) or pop this from the navigation controller (if the segue was push) and the previous controller will show automatically.
 
 For example, if the modal segue that goes back was being performed when you pressed a button on your login screen, you delete the segue from the storyboard and on the touch up inside event of the button you link with your action that, upon successful login will call:
 
 [self dismissViewControllerAnimated:YES completion:nil];
 On the storyboard, you will right click on the button and drag the touch up inside sent event to the first responder of the view controller scene. This will execute the reverse transition that was executed when you performed the segue transition. You can use a modal segue for that, no need to use a navigation controller.
 
 If you pushed the login view onto the navigation controller, then you have to pop it from the stack (if going back to the previous view). Use one of these:
 
 [[self navigationController] popViewControllerAnimated:YES];  // goes back to previous view
 [[self navigationController] popToViewController: myViewControllerAfterLogin animated:YES]; // goes back to specific view on stack.
 [[self navigationController] popToRootViewControllerAnimated:YES]; // goes back to first view on the stack
 
 #import <Foundation/Foundation.h>
 
 @interface Recipe : NSObject
 
 @property (nonatomic, strong) NSString *name; // name of recipe
 @property (nonatomic, strong) NSString *prepTime; // preparation time
 @property (nonatomic, strong) NSString *image; // image filename of recipe
 @property (nonatomic, strong) NSString *search; // type of food
 @property (nonatomic, strong) NSString *notes; // add a note to order
 @property (nonatomic, strong) NSString *quantityAsString; //new quant description
 
 @property (nonatomic, strong) NSArray *ingredients; // ingredients of recipe
 @property (nonatomic, strong) NSDecimalNumber* price; // price as a string
 
 
 //@property (nonatomic, assign) NSUInteger quantity;
 @property (nonatomic, strong) NSString *quantity;
 
 @property (nonatomic, assign) double weight;
 @property (nonatomic, strong) NSString *weightAsString; // the weight nicely formateted
 
 @property (nonatomic, strong) NSString *productCode; //replaces SKU
 @property (nonatomic, strong) NSString *sizing; //wether of not you can specify size info on order
 
 @end
 
*/



- (IBAction)lastJourneyPress:(id)sender {
    [self performSegueWithIdentifier:@"Home2LastJourney" sender:self];
}
- (IBAction)journeysListPress:(id)sender {
    [self performSegueWithIdentifier:@"Home2JourneysList" sender:self];
}
- (IBAction)statisticsPress:(id)sender {
    [self performSegueWithIdentifier:@"Home2Statistics" sender:self];
    
}
- (IBAction)fuelPress:(id)sender {
    [self performSegueWithIdentifier:@"Home2Fuel" sender:self];
}
- (IBAction)backButton:(id)sender {
}

@end
