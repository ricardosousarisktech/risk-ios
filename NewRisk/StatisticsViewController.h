//
//  StatisticsViewController.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatisticsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButton:(id)sender;

@end
