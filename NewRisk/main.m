//
//  main.m
//  NewRisk
//
//  Created by Mac Mini on 05/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
