//
//  HomeViewController.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>


@property (strong, nonatomic) IBOutlet UIButton *lastJourneyButton;
- (IBAction)lastJourneyPress:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *journeysListButton;
- (IBAction)journeysListPress:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *statisticsButton;
- (IBAction)statisticsPress:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *fuelButton;
- (IBAction)fuelPress:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *swipeWeekControl;

@property (strong, nonatomic) IBOutlet UILabel *weekLabel;

@property  (strong, nonatomic) NSArray* weekStrings;
@property   (nonatomic, assign) int weekOffset;
@property (strong, nonatomic) IBOutlet UIButton *numberPlateButton;
- (IBAction)numberPlatePressed:(id)sender;

//build up this list on viewDidAppear

@property (strong, nonatomic) NSMutableArray* weekRangeStrings;

//picker name
@property  (strong, nonatomic) NSArray* pickerData;
@property (strong, nonatomic) UIView *picker;
@property (strong, nonatomic) UIDatePicker *theDatePicker;
@property (strong, nonatomic) UIPickerView *IMEIpicker;


@end
