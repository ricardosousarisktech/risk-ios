//
//  Journey.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JourneyEvent.h"


@interface Journey : NSObject

@property (nonatomic, assign) JourneyEvent *journeyEvent;
@property (nonatomic, strong) NSArray *JourneyEvents;       //list of JourneyEvent objects
@property (nonatomic, strong) NSDecimalNumber *DistanceTravelled;
@property (nonatomic, strong) NSString *DistanceUnits;
@property (nonatomic, assign) NSUInteger RiskScore;
@property (nonatomic, assign) NSUInteger AccelScore;
@property (nonatomic, assign) NSUInteger DecelScore;
@property (nonatomic, assign) NSUInteger SpeedScore;
@property (nonatomic, strong) NSString *IdlingTime;

@end
