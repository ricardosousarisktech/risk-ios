//
//  SpeedingEvent.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpeedingEvent : NSObject


@property (nonatomic, assign) NSUInteger SpeedLimit;
@property (nonatomic, assign) NSUInteger EventCount;


@end
