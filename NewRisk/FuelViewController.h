//
//  FuelViewController.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FuelViewController : UIViewController
- (IBAction)backButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButton;


@property (strong, nonatomic) IBOutlet UILabel *distanceTravelledLabel;

@property (strong, nonatomic) IBOutlet UILabel *totalFuelSpendLabel;

@property (strong, nonatomic) IBOutlet UILabel *fuelScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *possibleSavingLabel;


@end
