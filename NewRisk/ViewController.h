//
//  ViewController.h
//  NewRisk
//
//  Created by Mac Mini on 05/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *companyLogo;

@property (strong, nonatomic) IBOutlet UITextField *userNameTextField;
- (IBAction)userNameTextFieldAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)passwordTextFieldAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginButton:(id)sender;


@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;

@end

