//
//  JourneyEvent.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JourneyEvent : NSObject

@property (nonatomic, assign) NSUInteger EventType;
@property (nonatomic, strong) NSString *EventTime;
@property (nonatomic, assign) double Latitude;
@property (nonatomic, assign) double Longitude;
@property (nonatomic, strong) NSDecimalNumber *EventSpeed;
@property (nonatomic, strong) NSString *SpeedLimit;
@property (nonatomic, strong) NSString *SpeedUnits;
@property (nonatomic, strong) NSString *StreetAddress;




@end
