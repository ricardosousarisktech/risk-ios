//
//  FuelViewController.m
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import "FuelViewController.h"
#import "FuelCost.h"

@interface FuelViewController ()

@property (retain, strong) FuelCost *stubFuelCost;

@end

@implementation FuelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //create a stub of data required for this screen
    
    self.stubFuelCost = [[FuelCost alloc] init];

    self.stubFuelCost.DistanceTravelled = [NSDecimalNumber decimalNumberWithString:@"100.5"];
    self.stubFuelCost.DistanceUnits = @"mi";
    self.stubFuelCost.TotalFuelSpend = [NSDecimalNumber decimalNumberWithString:@"63.67"];
    self.stubFuelCost.CurrentFuelScore = 86;
    self.stubFuelCost.PossibleSaving = [NSDecimalNumber decimalNumberWithString:@"6.67"];
    
    NSLog(@"created stub for fuel data");
    
    //nowupdate the view from this stub
    
    [self updateFromData:self.stubFuelCost];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButton:(id)sender {
    
    NSLog(@"Return from Fuel Screen");
    
    [[self navigationController] popViewControllerAnimated:YES];  // goes back to previous view
    
}

-(void)updateFromData:(FuelCost*)data{
    
    self.fuelScoreLabel.text =  [@(data.CurrentFuelScore) stringValue];
    self.possibleSavingLabel.text =  [NSString stringWithFormat:@"£%@", data.PossibleSaving]; //UNFINISED replace £ with correct currency
     self.totalFuelSpendLabel.text =  [NSString stringWithFormat:@"£%@", data.TotalFuelSpend]; //UNFINISED replace £ with correct currency
    self.distanceTravelledLabel.text =  [NSString stringWithFormat:@"%@ %@", data.DistanceTravelled, data.DistanceUnits];
    
    
    
}


@end
