//
//  FuelCost.h
//  NewRisk
//
//  Created by Mac Mini on 06/03/2015.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FuelCost : NSObject



@property (nonatomic, strong) NSDecimalNumber *DistanceTravelled;
@property (nonatomic, strong) NSString *DistanceUnits;
@property (nonatomic, strong) NSDecimalNumber *TotalFuelSpend;
@property (nonatomic, assign) NSUInteger CurrentFuelScore;
@property (nonatomic, strong) NSDecimalNumber *PossibleSaving;

@end
